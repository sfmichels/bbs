# BBS BaseBall Stats
 
 Calculate baseball stats using the open source Baseball Databank.
 
 This application can currently create batting statistics for every player who has played in the major leagues.
 The database is mysql.
 
 The code joins the master (Player) table with the Batting table and gets player and batting data.  It get reported
 data and calculates:
 
    * Batting Average - AVG
    * On Base Percentage - OBP
    * Total Bases - TB
    * Slugging - SLG
    * On Base Plus Slugging - OPS
 
 This ruby application is the work of [Alan Michels](mailto:sf.michels@gmail.com)
 
 The Baseball Databank is a separate open source project that can be found at: (www.baseball-databank.org).