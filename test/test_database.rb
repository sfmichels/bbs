gem "minitest"
require 'minitest/autorun'
require 'minitest/reporters'
require 'shoulda/context'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new
require_relative '../lib/database'

class TestDatabase < MiniTest::Test

  context 'database queries' do
    setup do
      @db = Database.new
      @db.connect
    end

    should 'get batting stats for a player from the database' do
      #testing actual database calls
      results = @db.batting_stats_by_name('Barry', 'Bonds')
      puts "Caller batting stats object id: " + results.object_id.to_s

      assert_equal 22, results.count
      # 16th season for Barry Bonds
      assert_equal 'Barry', results[15].first_name
      assert_equal 'Bonds', results[15].last_name
      assert_equal 2001, results[15].year
      assert_equal 1, results[15].stint
      assert_equal 'SFN', results[15].team
      assert_equal 153, results[15].games
      assert_equal 476, results[15].at_bats
      assert_equal 129, results[15].runs
      assert_equal 156, results[15].hits
      assert_equal 32, results[15].doubles
      assert_equal 2, results[15].triples
      assert_equal 73, results[15].home_runs
      assert_equal 137, results[15].rbis
      assert_equal 13, results[15].stolen_bases
      assert_equal 3, results[15].caught_stealing
      assert_equal 177, results[15].walks
      assert_equal 93, results[15].strike_outs
      assert_equal 35, results[15].intentional_walks
      assert_equal 9, results[15].hit_by_pitch
      assert_equal 0, results[15].sacrifice_hit
      assert_equal 2, results[15].sacrifice_fly
      assert_equal 5, results[15].ground_into_double_play
    end

    should 'not calculate any stats for a player not in the the database' do
      results = @db.batting_stats_by_name('Nota', 'RealPlayer')

      assert_equal 0, results.count
    end
  end
end

