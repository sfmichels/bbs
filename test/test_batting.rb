gem "minitest"
require "minitest-spec-context"
require 'minitest/autorun'
require 'minitest/reporters'
require 'shoulda/context'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require_relative '../lib/batting'

class TestBatting < MiniTest::Test

  context 'calculate batting stats' do
    setup do
      @batting = Batting.new
    end

    should '4 at bats and 1 hit should be 0.250' do
      at_bats = 4
      hits = 1

      assert_equal 0.250, @batting.calculate_average(hits, at_bats)
    end

    should '3 at bats and 1 hit should be close to 0.333' do
      at_bats = 3
      hits = 1

      assert_in_delta 0.333, @batting.calculate_average(hits, at_bats), 0.001
    end

    should '3 at bats and 2 hits should be close to 0.667' do
      at_bats = 3
      hits = 2

      assert_in_delta 0.667, @batting.calculate_average(hits, at_bats), 0.001
    end

    should 'calculate on base percentage' do
      at_bats = 100
      hits = 40
      walks = 10
      hit_by_pitch = 5
      sacrifice_fly = 10

      assert_equal 0.440, @batting.calculate_onbase_percentage(at_bats, hits, walks, hit_by_pitch, sacrifice_fly)
    end

    should 'calculate on base percentage with no at bats should be 0' do
      at_bats = 0
      hits = 0
      walks = 0
      hit_by_pitch = 0
      sacrifice_fly = 0

      assert_equal 0.0, @batting.calculate_onbase_percentage(at_bats, hits, walks, hit_by_pitch, sacrifice_fly)
    end

    context 'total bases for different types of hits' do
      # calculate_total_bases(hits, doubles, triples, home_runs)

      # Note: baseball stats don't list the number of singles
      # Singles are all hits that aren't doubles, triples or home runs
      # singles = hits - doubles - triples - home_runs

      should 'calculate 0 total bases for 0 hits' do
        assert_equal 0, @batting.calculate_total_bases(0, 0, 0, 0)
      end

      should 'calculate 10 total bases for 10 singles' do
        assert_equal 10, @batting.calculate_total_bases(10, 0, 0, 0)
      end

      should 'calculate 8 total bases for 4 doubles' do
        assert_equal 8, @batting.calculate_total_bases(4, 4, 0, 0)
      end

      should 'calculate 9 total bases for 3 triples' do
        assert_equal 9, @batting.calculate_total_bases(3, 0, 3, 0)
      end

      should 'calculate 8 total bases for 2 home runs' do
        assert_equal 8, @batting.calculate_total_bases(2, 0, 0, 2)
      end

      should 'calculate 26 total bases for 10 singles, 3 doubles, 2 triples and 1 home run' do
        assert_equal 26, @batting.calculate_total_bases(16, 3, 2, 1)
      end
    end

    context 'on base plus slugging requires other values to be calculated first' do

      should 'calculate 0.400 for onbase_plus_slugging with 20 at_bats, 5 singles, 1 double, 1 triple, 1 home run' do
        @batting.onbase_percentage = 0.400
        @batting.slugging = 0.700

        assert_in_delta 1.100, @batting.calculate_onbase_plus_slugging(), 0.001
      end

      should 'calculate on base + slugging if "on base" has been calculated but not slugging value should be ' do
        @batting.at_bats = 100
        @batting.hits = 30
        @batting.doubles = 5
        @batting.triples = 1
        @batting.home_runs = 4
        @batting.walks = 10
        @batting.hit_by_pitch = 2
        @batting.sacrifice_fly = 3

        assert_in_delta 0.855, @batting.calculate_onbase_plus_slugging(), 0.001
      end

    end

  end
end


