require_relative 'database'
require_relative 'batting'

class BattingStats
  attr_accessor :stats

  def initialize
    @db = Database.new
    @db.connect

    @stats = []
  end

  def get_player_stats(first_name, last_name)
    stats = @db.batting_stats_by_name(first_name, last_name)
    puts "Number of rows: #{stats.count}"
    stats.each do |batting|
      puts batting.inspect
    end
  end

  def get_all_stats
    start_time = Time.now
    stats = @db.batting_stats_all_players_all_years
    end_time = Time.now
    puts "Number of rows: #{stats.count} in #{end_time - start_time} seconds"
  end


  b = BattingStats.new
  #b.get_player_stats('Barry', 'Bonds')
  b.get_all_stats

  puts "Goodbye"

end
