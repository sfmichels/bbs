require 'mysql'
require_relative 'batting'
class Database

# Connect to database:
  def connect
    @con = Mysql.new 'localhost', 'alan', 'alanpw',  'lahman'
  end

  SELECT_DB_BATTING_VALUES =
      <<-SQL_BATTING_VALUES
        SELECT  m.nameFirst, m.nameLast,
                b.yearID, b.stint, b.teamID,
                b.G, b.AB, b.R, b.H, b.2B, b.3B, b.HR,
                b.RBI, b.SB, b.CS, b.BB, b.SO, b.IBB,
                b.HBP, b.SH, b.SF, B.GIDP
      SQL_BATTING_VALUES

  SQL_BY_PLAYER_NAME_ALL_YEARS =
      <<-SQL_BY_PLAYER_NAME
        FROM batting b JOIN master m ON m.playerID=b.playerID
        WHERE m.nameFirst=? AND m.nameLast=?
        ORDER BY b.yearID ASC, b.stint ASC
  SQL_BY_PLAYER_NAME


  def batting_stats_by_name(first_name, last_name)
    statement = SELECT_DB_BATTING_VALUES + ' ' + SQL_BY_PLAYER_NAME_ALL_YEARS

    pst = @con.prepare statement
    data = pst.execute first_name, last_name
    results = []
    data.each do |row|
      batting = Batting.new()
      batting.first_name = row[0]
      batting.last_name = row[1]
      batting.year = row[2]
      batting.stint = row[3]
      batting.team = row[4]
      batting.games = row[5]
      batting.at_bats = row[6]
      batting.runs = row[7]
      batting.hits = row[8]
      batting.doubles = row[9]
      batting.triples = row[10]
      batting.home_runs = row[11]
      batting.rbis = row[12]
      batting.stolen_bases = row[13]
      batting.caught_stealing = row[14]
      batting.walks = row[15]
      batting.strike_outs = row[16]
      batting.intentional_walks = row[17]
      batting.hit_by_pitch = row[18]
      batting.sacrifice_hit = row[19]
      batting.sacrifice_fly = row[20]
      batting.ground_into_double_play = row[21]

      results << batting
    end
    results
  end

  SQL_ALL_PLAYERS_ALL_YEARS =
      <<-ALL_PLAYERS_ALL_YEARS
        FROM batting b JOIN master m ON m.playerID=b.playerID
        ORDER BY b.yearID ASC, b.stint ASC
      ALL_PLAYERS_ALL_YEARS


  def batting_stats_all_players_all_years
    statement = SELECT_DB_BATTING_VALUES + ' ' + SQL_ALL_PLAYERS_ALL_YEARS

    pst = @con.prepare statement
    data = pst.execute
    results = []
    data.each do |row|
      batting = Batting.new()
      batting.first_name = row[0]
      batting.last_name = row[1]
      batting.year = row[2]
      batting.stint = row[3]
      batting.team = row[4]
      batting.games = row[5]
      batting.at_bats = row[6]
      batting.runs = row[7]
      batting.hits = row[8]
      batting.doubles = row[9]
      batting.triples = row[10]
      batting.home_runs = row[11]
      batting.rbis = row[12]
      batting.stolen_bases = row[13]
      batting.caught_stealing = row[14]
      batting.walks = row[15]
      batting.strike_outs = row[16]
      batting.intentional_walks = row[17]
      batting.hit_by_pitch = row[18]
      batting.sacrifice_hit = row[19]
      batting.sacrifice_fly = row[20]
      batting.ground_into_double_play = row[21]

      results << batting
    end
    results
  end

end
