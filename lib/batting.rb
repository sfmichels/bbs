class Batting
  # recorded values
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :year
  attr_accessor :stint
  attr_accessor :team
  attr_accessor :league
  attr_accessor :games
  attr_accessor :games_batting
  attr_accessor :at_bats
  attr_accessor :runs
  attr_accessor :hits
  attr_accessor :doubles
  attr_accessor :triples
  attr_accessor :home_runs
  attr_accessor :rbis
  attr_accessor :stolen_bases
  attr_accessor :caught_stealing
  attr_accessor :walks
  attr_accessor :strike_outs
  attr_accessor :intentional_walks
  attr_accessor :hit_by_pitch
  attr_accessor :sacrifice_hit
  attr_accessor :sacrifice_fly
  attr_accessor :ground_into_double_play

  # calculated values
  attr_accessor :average
  attr_accessor :onbase_percentage
  attr_accessor :total_bases
  attr_accessor :slugging

  def calculate_average(hits, at_bats)
    at_bats > 0 ? hits.to_f / at_bats : 0.0
  end

  def calculate_onbase_percentage(at_bats, hits, walks, hit_by_pitch, sacrifice_fly)
    at_bats > 0 ? (hits.to_f + walks + hit_by_pitch) / (at_bats.to_f + walks + hit_by_pitch + sacrifice_fly) : 0.0
  end

  def calculate_total_bases(hits, doubles, triples, home_runs)
    hits + doubles + 2 * triples + 3 * home_runs
  end

  def calculate_slugging(at_bats, hits, doubles, triples, home_runs)
    calculate_total_bases(hits.to_f, doubles, triples, home_runs) / at_bats
  end

  def calculate_onbase_plus_slugging()
    if @onbase_percentage.nil?
      @onbase_percentage = calculate_onbase_percentage(@at_bats, @hits, @walks, @hit_by_pitch, @sacrifice_fly)
    end

    if @slugging.nil?
      @slugging = calculate_slugging(@at_bats, @hits, @doubles, @triples, @home_runs)
    end

    @onbase_percentage + @slugging
  end

  def set_calculated_stats
    @average = calculate_average(hits, at_bats)
    @total_bases = calculate_total_bases(at_bats, doubles, triples, home_runs)
    @onbase_percentage = calculate_onbase_percentage(at_bats, hits, walks, hit_by_pitch, sacrifice_fly)
    @slugging = calculate_slugging(at_bats, hits, doubles, triples, home_runs)
    @onbase_plus_slugging = calculate_onbase_plus_slugging()
  end

end

